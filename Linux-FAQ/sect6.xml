  <sect1 id="common-problems">
    <title id="common-problems-title">Giải pháp cho các vấn đề linh
tinh thông thường</title>
    <qandaset defaultlabel='qanda'>
      <qandaentry>
	<question id="ppp-connection-dies-when-sending"><para>Tại sao
FTP dường như bị treo?</para></question>
	<answer>
	  <para>FTP bị chết đột ngột thường do tràn vùng đệm. Trên các
hệ Linux, vấn đề này dường như xuất hiện thường xuyên nhất với các
phần mềm server trong các bản phân phối.</para>
	  <para>Nếu bạn nhận được lỗi <literal>ftp: connection
refused</literal>, thường là do thiếu xác thực (authentication). Hãy
tham khảo <quote><xref  linkend="ftp-wont-login"		/></quote>.  </para>
	  <para>Một giải pháp là thay thế các server FTP trong các bản
phân phối bằng server FTP của OpenBSD. Trang chủ của chương trình là:
<ulink
url='http://www.eleves.ens.fr:8080/home/madore/programs/'><citetitle>http://www.eleves.ens.fr:8080/home/madore/programs/</citetitle></ulink>.

</para>
	  <para>Để cài đặt server này, hãy theo các chỉ dẫn cài đặt,
và tham khảo man page của <filename>inetd</filename> và [inetd.conf].
(Nếu bạn chưa quen <filename>xinetd</filename>, hãy xem bên dưới.) Hãy
kiểm tra để chắc chắn cho [inetd] biết cần chạy BSD daemon độc lập,
không phải là một tiến trình con, ví dụ như là tiến trình con của
[tcpd]. Comment những dòng bắt đầu bằng
<quote><literal>ftp</literal></quote> trong tập tin
/etc/inetd.conf<filename></filename> và thay bằng một dòng tương tự
như sau (nếu bạn cài đặt bản [ftpd] mới trong
<filename>/usr/local/sbin/</filename>):
</para>
	  <para>
 <programlisting> # Original entry, commented out. #ftp stream tcp
nowait root /usr/sbin/tcpd /usr/sbin/in.ftpd # Replacement entry: ftp
stream tcp nowait root /usr/local/sbin/ftpd -l </programlisting>
 </para>
	  <para>Daemon thay thế sẽ hoạt động sau khi khởi động lại
inetd hoặc gửi tín hiệu (với quyền root) <literal>SIGHUP</literal> cho
<literal>inetd</literal>, v.d.:  </para>
	  <para>
 <screen> # kill -HUP inetd</screen>
 </para>
	  <para>Để cấu hình [xinetd], hãy tạo một mục trong
<filename>/etc/xinetd.d</filename> per the instructions in the
<filename>xinetd.conf</filename> manual page. Hãy kiểm tra để chắc
chắn các tham số cho [ftpd] là đúng, và bạn đã cài tập tin
<filename>/etc/ftpusers</filename> và
<filename>/etc/pam.d/ftp</filename>. Sau đó hãy khởi động lại [xinetd]
bằng lệnh: <literal>/etc/rc.d/init.d/xinetd restart</literal>. Lệnh
này sẽ báo "OK", system message log sẽ lưu thông báo này.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="free-dumps-core"><para>Tại sao
<command>free</command> tạo core?</para></question>
	<answer><para>Với Linux 1.3.57 trở đi, dạng thức của
<filename>/proc/meminfo</filename> đã bị thay đổi nên
<command>free</command> không hiểu.  </para>
	  <para>Hãy lấy phiên bản <command>free</command> mới nhất, từ
metalab.unc.edu, trong
<filename>/pub/Linux/system/Status/ps/procps-0.99.tgz</filename>.
</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="netscape-crashes"><para>Tại sao Netscape bị hỏng
thường xuyên?</para></question>
	<answer>
	  <para>Netscape khó bị crash nếu nó được cấu hình đúng, và
cấu hình mạng cũng đúng. Vài thứ cần kiểm tra là: </para>

	  <itemizedlist>
	    <listitem><para>Hãy kiểm tra biến môi trường
<literal>MOZILLA_HOME</literal> xem có đúng không. Nếu bạn cài
Netscape trong  <filename>/usr/local/netscape/</filename>, ví dụ, vậy
thì đó cũng là giá trị của <literal>MOZILLA_HOME</literal>.  Hãy đặt
biến môi trường bằng dòng lệnh (v.d,  "<literal>export
MOZILLA_HOME="/usr/local/netscape"</literal>" trong
<command>bash</command> hoặc thêm dòng đó vào tập tin khởi động hệ
thống hoặc tập tin khởi động của người dùng. Hãy tham khảo man page
của shell để biết cách dùng lệnh.</para>
	    </listitem>
	    <listitem><para>Nếu bạn dùng phiên bản brand-new, hãy thử
với các phiên bản cũ hơn, đề phòng trường hợp không tương thích thư
viện run-time. Ví dụ, Netscape 4.75 được cài (gõ  "<literal>netscape
--version</literal>" tại dấu nhắc shell), hãy thử cài bản 4.7. Mọi
phiên bản được lưu tại <ulink
url="ftp://ftp.netscape.com/">ftp://ftp.netscape.com/</ulink>.
</para>
	    </listitem>
	    <listitem><para>Netscape dùng thư viện Motif và Java
Runtime Environment (JRE) riêng. Nếu có một phiên bản khác của các thư
viện này đã được cài trên máy bạn trước đó, hãy đảm bảo rằng chùng
không interfer with Netscape's libraries; v.d., bằng cách gỡ bỏ chúng.
 </para>
	    </listitem>
	    <listitem><para>Hãy chắc chắn rằng Netscape có thể kết nối
tới name server mặc định của nó. Nếu chương trình có vẻ ngừng hoạt
động trong vài phút thì có lẽ là nó không kết nối được tới name server
mặc định của nó, nghĩa là hệ thống không thể kết nối tới các máy
khác.</para>
	    </listitem>
	  </itemizedlist>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="ftp-wont-login"><para>Tại sao FTP hoặc Telnet
server của tôi không cho phép đăng nhập?</para></question>
	<answer>
	  <para>
Những điều sau đây chỉ áp dụng cho các server có trả lời cho client,
nhưng lại không cho phép đăng nhập. Trên các hệ thống mới có cài
Pluggable Authentication Module (PAM), hãy xem các tập tin tên là
[ftp] hoặc [telnet] trong thư mục <filename>/etc/pam/</filename> hoặc
trong <filename>/etc/pam.d/</filename>. Nếu tập tin tương ứng không
tồn tại, hãy xem chỉ dẫn cấu hình xác thực FTP và Telnet và các cấu
hình PAM khác, đặt ở /usr/doc/pam-&lt;version&gt;]. Ngoài ra hãy tham
khảo câu trả lời cho "<xref linkend="ftp-421-error"	      />."  </para>
	  <para>
Nếu FTP server đặt trên một hệ thống cũ, hãy kiểm tra xem tài khoản
được dùng có tồn tại trong tập tin <filename>/etc/passwd</filename>
hay không, đặc biệt là tài khoản
<quote><literal>anonymous</literal></quote>.  </para>
	  <para>
Loại lỗi này có thể do lỗi phân giải địa chỉ máy, đặt biệt nếu bạn
dùng Reverse Address Resolution Protocol (RARP). Câu trả lời đơn giản
là liệt kê toàn bộ tên máy có liên quan và địa chỉ IP của máy đó vào
trong tập tin <filename>/etc/hosts</filename> trên mỗi máy. (Hãy xem
ví dụ về tập tin <filename>/etc/hosts</filename> và
<filename>/etc/resolv.conf</filename> tại: "<xref	      linkend="sendmail-pause" />.")
Nếu
mạng có DNS nội bộ, hãy kiểm tra để chắc chắn mỗi máy có thể phân giải
địa chỉ mạng bằng DNS đó. </para>
	  <para>Nếu máy hoàn toàn không trả lời FTP hoặc Telnet client
thì có lẽ daemon của server đã được cài đặt không đúng, hoặc chưa cài
đặt. Hãy tham khảo man page: <literal>inetd</literal> và
<literal>inetd.conf</literal> trên các hệ thống cũ, or
<literal>xinetd</literal>  và <literal>xinetd.conf</literal>, cũng như
<literal>ftpd</literal>, và  <literal>telnetd</literal>.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="track-of-bookmarks-netscape"><para>Làm sao theo
dõi Bookmark trong Netscape?</para></question>
	<answer><para>Điều này có lẽ áp dụng cho hầu hết các trình
duyệt khác luôn. Trong menu Preferences/Navigator, đặt trang chủ của
Netscape là <filename>bookmarks.html</filename>, nằm trong thư mục
<filename>.netscape</filename> (có dấu chấm ở đầu). Ví dụ, nếu tên
đăng nhập của bạn là <quote><literal>smith</literal></quote>, hãy đặt
trang chủ là:  </para>
	  <para>
 <screen> file://home/smith/.netscape/bookmarks.html</screen>
 </para>
	  <para>Thiết lập trang chủ như trên sẽ thể hiện một trang
bookmark đẹp khi Netscape khởi động, và được tự động cập nhật bất cứ
khi nào bạn thêm, xóa hoặc thăm một site nào đó được bookmark.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="computer-has-wrong-time"><para>Tai sao máy tính
lưu sai giờ?</para></question>
	<answer>
	  <para>Có hai đồng hồ trong máy tính. Đồng hồ phần cứng
(CMOS) luôn chạy kể cả khi tắt máy. Đồng hồ này được dùng khi hệ thống
khởi động và được dùng bởi DOS (nếu bạn dùng DOS). Thời gian hệ thống,
được hiển thị và sửa đổi bằng lệnh <command>date</command>, được hạt
nhân quản lý trong khi Linux đang chạy.</para>
	  <para>Bạn có thể hiển thị thời gian của đồng hồ CMOS, hoặc
đặt giờ của đồng hồ này nhờ cái còn lại, bằng
<filename>/sbin/clock</filename>  (bây giờ được gọi là
<command>hwclock</command> trong nhiều bản phân phối). Hãy tham khảo:
<command>man 8 clock</command> hoặc <command>man 8 hwclock</command>.
</para>
	  <para>Có nhiều chương trình khác nhau có thể sửa một trong
hai hoặc cả hai đồng hồ for system drift hoặc truyền thời gian xuyên
mạng. Vài chương trình trong số đó có thể đã được cài trên hệ thống
của bạn. Hãy thử tìm <command>adjtimex</command> (corrects for drift),
các client Giao thức Thời gian Mạng (Network Time Protocol - NTP) như
<command>netdate</command>, <command>getdate</command>, và
<command>xntp</command>, hoặc bộ client server NTP như
<command>chrony</command>. Hãy tham khảo: <quote><xref
		linkend="ported-compiled-written-xxx"		/></quote>.
</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="setuid-scripts-dont-work"><para>Tại sao script
có setuid không hoạt động?</para></question>
	<answer>
	  <para>Chúng không được thiết kế để hoạt động được. Tính năng
này đã được vô hiệu hóa trong hạt nhân Linux, vì các script có setuid
rất dễ tạo ra lỗ hổng bảo mật. <application>Sudo</application> và
<application>SuidPerl</application> có thể cung cấp một cơ chế an toàn
hơn so với các script hoặc chương trình có setuid, đặc biệu trong
trường hợp quyền thực thi được giới hạn cho một người dùng hoặc một
nhóm người dùng.</para>
	  <para>Nếu bạn muốn biết tại sao script có setuid tạo ra lỗ
hổng bảo mật, hãy đọc FAQ của
<citetitle>comp.unix.questions</citetitle>.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="free-memory-keeps-shrinking"><para>Tại sao lượng
bộ nhớ trống (Free Memory) được báo cáo bởi <command>free</command>
ngày càng ít?</para></question>
	<answer><para>Lượng bộ nhớ <quote>trống</quote> in bởi
<command>free</command> không tính lượng bộ nhớ được dùng là vùng đệm
đĩa, được hiển thị trong cột
<quote><literal>buffers</literal></quote>. Nếu bạn muốn biết thực sự
còn trống bao nhiêu bộ nhớ, hãy cộng phần bộ nhớ trong
<quote><literal>buffers</literal></quote> vào khoảng
<quote><literal>free</literal></quote>. Các phiên bản
<command>free</command> mới hơn in dòng thông tin mở rộng chứa thông
tin này.</para>
	  <para> Vùng đệm đĩa có khuynh hướng tăng nhanh sau khi Linux
khởi động. Khi bạn nạp chương trình và đọc tập tin, chúng sẽ được
cache. Tuy nhiên lượng cache sẽ trở nên ổn định sau một khoảng thời
gian.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="add-memory-system-slows"><para>Tại sao hệ thống
chậm đi khi thêm bộ nhớ?</para></question>
	<answer><para>Đây là triệu chứng thông thường khi gặp lỗi cách
bộ nhớ bổ sung. Vấn đề chính xác phụ thuộc vào motherboard của
bạn.</para>
	  <para>Đôi khi bạn bật cache trên một số vùng nhất định trong
thiết lập BIOS. Hãy xem lại thiết lập CMOS xem có tùy chọn nào cache
vùng nhớ mới, mà hiện chưa được bật không. Điều này xảy ra thường
xuyên với 486.</para>
	  <para>Đôi khi RAM phải được cắm vào đúng socket mới có thể
được cache.</para>
	  <para>Đôi khi bạn phải đặt jumper để bật cache.</para>
	  <para>Đôi khi motherboards không cache toàn bộ RAM nếu bạn
có nhiều RAM trên mỗi lượng cache hơn dự đoán. Thường full cache  256K
sẽ xử lý vấn đề này.</para>
	  <para>Nếu nghi ngờ, hãy xem lại tài liệu.  Nếu bạn vẫn không
thể sửa lỗi vì tài liệu không thích hợp, bạn có thể gửi thông báo lên
<citetitle>comp.os.linux.hardware</citetitle>, cung cấp đầy đủ thông
tin chi tiết,  số model, date code, ..., nhờ đó người khác có thể
tránh dùng nó.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="some-programs-wont-log-in"><para>Tại sao vài
chương trình (v.d. <command>xdm</command>) không cho phép đăng
nhập?</para></question>
	<answer><para>Có lẽ bạn dùng chương trình dùng mật khẩu
non-shadow trong khi bạn lại đang dùng mật khẩu shadow.</para>
	  <para>Nếu thế, bạn cần phải lấy một phiên bản mới dùng mật
khẩu shadow và biên dịch lại. Bộ chương trình mật khẩu shadow đặt tại
<ulink
url="ftp://tsx-11.mit.edu:/pub/linux/sources/usr.bin/shadow/">ftp://tsx-11.mit.edu:/pub/linux/sources/usr.bin/shadow/</ulink>.
Đó là mã nguồn.  Mã nhị phân có lẽ trong [linux/binaries/usr.bin/].
</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="log-in-with-no-password"><para>Tại sao vài
chương trình cho phép đăng nhập không cần mật khẩu?</para></question>
	<answer>
	  <para>Có lẽ bạn gặp vấn đề như (<quote><xref
		linkend="some-programs-wont-log-in"		/></quote>), cộng
thêm vài thứ khác.  </para>
	  <para>Nếu bạn dùng mật khẩu shadow, bạn nên thêm ký tự
<literal>x</literal> hoặc một dấu sao vào trường mật khẩu của mỗi tài
khoản trong tập tin <filename>/etc/passwd</filename>, nhờ đó nếu một
chương trình nào đó không biết mật khẩu shadow, nó sẽ không nghĩ rằng
đó là tài khoản không có mật khẩu rồi cho phép đăng nhập tự do.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="machine-runs-very-slowly-when"><para>Tại sao máy
chạy rất chậm với GCC / X / ...?</para></question>
	<answer>
	  <para>Có lẽ do bạn có quá ít bộ nhớ. Nếu bạn có ít RAM hơn
lượng cần thiết cho các chương trình bạn đang chạy, Linux sẽ swap đĩa
cứng của bạn và chạy chậm kinh khủng. Giải pháp trong trường hợp này
là đừng chạy cùng lúc quá nhiều chương trình hoặc mua thêm RAM. Bạn
cũng có thể tăng bộ nhớ bằng cách biên dịch hạt nhân với ít tùy chọn
hơn. Hãy xem (<quote><xref linkend="upgrade-recompile-kernel"		/></quote>)  </para>
	  <para>Bạn có thể biết đang dùng bao nhiêu bộ nhớ và vùng
hoán đổi bằng lệnh <command>free</command>, hoặc bằng cách gõ: </para>
	  <para>
 <screen> $ cat /proc/meminfo</screen>
 </para>
	  <para>Nếu hạt nhân của bạn được cấu hình với RAM disk, có
thể điều đó làm phí phạm bộ nhớ và làm cho hệ thống chậm đi. Hãy dùng
LILO hoặc rdev để bảo hạt nhân đường tạo RAM disk (hãy xem tài liệu
LILO hoặc gõ <quote><literal>man rdev</literal></quote>).  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="can-only-log-in-as-root"><para>Tại sao hệ thống
chỉ cho phép đăng nhập root?</para></question>
	<answer>
	  <para>Có thể bạn đang gặp vấn đề về quyền truy cập, hoặc bạn
có tập tin <filename>/etc/nologin</filename>. </para>
	  <para>Trong trường hợp sau, hãy đặt dòng <quote><literal>rm
-f /etc/nologin</literal></quote> vào tập tin
<filename>/etc/rc.local</filename> hoặc
<filename>/etc/rc.d/*</filename> của bạn.</para>
	  <para>Trong trường hợp khác, hãy kiểm tra lại quyền truy cập
của shell của bạn và bất kỳ tập tin nào xuất hiện trong thông báo lỗi,
và cả các thư mục chứa những tập tin đó cho tới thư mục gốc.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="screen-is-all-full-of-weird"><para>Tại sao màn
hình đầy những ký tự kỳ lạ?</para></question>
	<answer>
	  <para>Có lẽ do bạn đã lỡ gửi dữ liệu nhị phân ra màn hình.
Hãy gõ <literal>echo -e '\ec'</literal> để sửa lỗi. Nhiều bản phân
phối có lệnh <command>reset</command> để làm công việc tương tự.
</para>
	  <para>Nếu vẫn không được, hãy thử gửi lệnh escape trực tiếp
ra màn hình. </para>
	  <para>
 <screen> $ echo '<keycombo><keycap>Ctrl</keycap><keycap>V</keycap></keycombo><keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo>'</screen>
 </para>
	  <para>Việc này phục hồi font mặc định của console. Nhớ giữ
phím Control và gõ ký tự thay vì ấn phím Control, buông ra, rồi mới gõ
ký tự. Chuỗi lệnh  </para>
	  <para>
 <screen> $ echo '<keycombo><keycap>Ctrl</keycap><keycap>V</keycap></keycombo><keycap>Esc</keycap> <keycap>C</keycap>'</screen>
 </para>
	  <para>reset toàn màn hình. If there's data left on the shell
command line after typing a binary file, nhấn
<keycombo><keycap>Ctrl</keycap><keycap>C</keycap></keycombo> vài lần
để phục hồi dấu nhắc shell.  </para> 
	  <para>Lệnh khác có thể dùng là
một bí danh<footnote> <para>alias</para> </footnote>,
<quote><literal>sane</literal></quote>, có thể làm việc với các
terminal thông thường: </para>
	  <para> <screen> $ alias sane=`echo -e "c";tput is2; > stty sane line 1 rows $LINES columns $COLUMNS`</screen>
</para>
	  <para>Lệnh trên dùng dấu nháy ngược (kế bên phím số 1),
không phải nháy đơn. Xuống hàng chỉ để cho rõ ràng, không cần thiết
phải làm như vậy.</para> <para>Hãy bảo đảm rằng
<literal>$LINES</literal> và
<literal>$COLUMNS</literal> đã được định nghĩa trong [~/.cshrc]  hoặc
[~/.bashrc] bằng một lệnh giống như lệnh sau:  </para>
	  <para> <screen>$ LINES=25; export $LINES; $COLUMNS=80; export $COLUMNS</screen>
</para>
	  <para>Đặt <literal>$LINES</literal> và
<literal>$COLUMNS</literal> đúng bằng số dòng và số cột của terminal
của bạn.  </para>
	  <para>Cuối cùng, đầu ra của <quote><command>stty
-g</command></quote> có thể được dùng để tạo shell script để reset
terminal:  </para>

	  <orderedlist>
	    <listitem><para>Lưu kết quả đầu ra của
<quote><command>stty -g</command></quote> vào tập tin. Trong ví dụ
này, tập tin đó là <quote>termset</quote>.: </para>
	      <screen> $ stty -g >termset </screen>

	      <para>Kết quả của <quote><command>stty
-g</command></quote> (nội dung của <quote>[termset]</quote>) sẽ trông
giống thế này:  </para>
	      <screen>500:5:bd:8a3b:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:73</screen>

	    </listitem>
	    <listitem><para>Sửa <quote>[termset]</quote> thành shell
script bằng cách thêm interpreter và lệnh
<quote><command>stty</command></quote>:  </para>
	      <screen> #!/bin/bash stty 500:5:bd:8a3b:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:73</screen>

	    </listitem>
	    <listitem><para>Đặt quyền thực thi cho
<quote><command>termset</command></quote> và dùng như một shell
script: </para>
	      <screen> $ chmod +x termset
 $ ./termset</screen>

	    </listitem>
	  </orderedlist>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="screwed-up-system-and-cant-log-in"><para>Nếu tôi
quậy hệ thống và không thể đăng nhập được thì làm thế nào để khắc
phục?</para></question>
	<answer>
	  <para>
Bạn đã tạo đĩa mềm khẩn cấp rồi, phải không? Hãy khởi động từ đĩa mềm
đó. Ví dụ, cặp đĩa mềm của Slackware (gồm đĩa khởi động và đĩa root)
nằm trong thư mục install của bản phân phối Slackware.</para>
	</answer>
	<answer>
	  <para>
Ngoài ra còn có hai gói phần mềm tự tạo đĩa khẩn cấp ở <ulink
url="ftp://metalab.unc.edu/pub/Linux/system/Recovery/">ftp://metalab.unc.edu/pub/Linux/system/Recovery/</ulink>.
Những cái này tốt hơn vì chúng chứa luôn hạt nhân trên đĩa, vì thế bạn
không phải lo nguy cơ thiếu các thiết bị và hệ thống tập tin.</para>
	  <para>
Hãy mở shell và gắn kết ổ đĩa cứng bạn bằng dòng lệnh tương tự như
</para>
	  <para>
 <screen> $ mount -t ext2 /dev/hda1 /mnt</screen>
 </para>
	  <para>Sau đó hệ thống tập tin của bạn sẽ được gắn với thư
mục <filename>/mnt</filename> và bạn có thể sửa lỗi. Hãy nhớ tháo gắn
kết đĩa cứng trước khi khởi động lại (<command>cd</command> ra ngoài
trước, nếu không nó sẽ báo <quote>busy</quote>).  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="forgot-root-password"><para>Điều gì sẽ xảy ra
nếu tôi quên mật khẩu <literal>root</literal>?</para></question>
	<answer>
	  <para>
<warning><para>Một sai phạm trong việc hiệu chỉnh những tập tin trong
thư mục <filename>/etc</filename> có thể làm hỏng hệ thống bạn. Hãy
tạo một bản sao của bất cứ tập tin nào bạn định sửa để đề phòng sai
sót.</para></warning></para>
	  <para>Nếu bản phân phối của bạn cho phép khởi động chế độ
người dùng đơn (single-user), hãy thử khởi động chế độ này bằng cách
gõ <quote><literal>single</literal></quote> tại dấu nhắc <literal>BOOT
lilo:</literal>. Nhiều bản phân phối gần đây cho phép khởi động chế độ
người dùng đơn bằng cách gõ <quote><literal>linux
1</literal></quote>, <quote><literal>linux single</literal></quote>,
hoặc  <quote><literal>init=/bin/bash</literal></quote>.  </para>
	  <para>Nếu cách trên không được, hãy khởi động từ đĩa cài đặt
hoặc đĩa mềm, và chuyển sang console ảo khác bằng
<keycombo><keycap>Alt</keycap><keycap>F1</keycap></keycombo> --
<keycombo><keycap>Alt</keycap><keycap>F8</keycap></keycombo>, và sau
đó gắn kết hệ thống tập tin gốc vào <literal>/mnt</literal>. Sau đó
thực hiện những bước dưới đây để xác định xem hệ thống bạn dùng mật
khẩu
chuẩn hãy mật khẩu shadow, và làm cách nào để bỏ mật khẩu.</para>
	  <para>Dùng trình soạn thảo ưa thích của bạn để sửa mục root
trong tập tin <filename>/etc/passwd</filename> để bỏ mật khẩu. Mật
khẩu được đặt giữa dấu hai chấm đầu và dấu hai chấm thứ hai.
<quote>Chỉ làm vậy trừ khi trường mật khẩu chỉ chứa một ký tự
<quote><literal>x</literal></quote>. Trong trường hợp đó, hãy xem bên
dưới</quote>  </para>
	  <para>
 <screen> root:Yhgew13xs:0:0: ...</screen>
 </para>
	  <para>Đổi thành: </para>
	  <para>
 <screen> root::0:0: ...</screen>
 </para>
	  <para>Nếu mật khẩu chỉ chứa một ký tự
<quote><literal>x</literal></quote>, bạn phải bỏ mật khẩu trong tập
tin <filename>/etc/shadow</filename>, có cùng dạng thức với tập tin
<filename>/etc/passwd</filename>. Hãy tham khảo man page:
<quote><literal>man passwd</literal></quote>, và  <quote><literal>man
5 shadow</literal></quote>.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="a-security-hole-in-rm"><para>Đâu là lỗ hổng bảo
mật nghiêm trọng của lệnh <command>rm</command>!?!?!</para></question>
	<answer><para>Không có. Rõ ràng bạn cò lạ lẫm với các hệ điều
hành họ Unix và cần đọc một cuốn sách để hiểu mọi thứ hoạt động ra
sao. Đầu mối: khả năng xóa tập tin phụ thuộc vào quyền truy cập ghi
của thư mục đó.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="lpr-andor-lpd-dont-work"><para>Tại sao
<command>lpr</command> và/hoặc <command>lpd</command> không hoạt
động?</para></question>
	<answer>
	  <para>Trước hết, hãy kiểm tra để chắc chắn
<filename>/dev/lp*</filename> được cấu hình đúng. IRQ và địa chỉ cổng
cần phải đúng với các thiết lập trên card máy in. Bạn sẽ có thể xuất
một tập tin trực tiếp ra máy in:</para>
	  <para>
 <screen> $ cat the_file >/dev/lp1</screen>
 </para>
	  <para>Nếu <command>lpr</command>  đưa ra thông báo như
<quote><literal>myname@host: host not found</literal></quote>, có
nghĩa là có khả năng giao diện loopback,  <literal>lo</literal>, không
hoạt động. Hỗ trợ loopback có trong hầu hết các bản phân phối của hạt
nhân. Hãy kiểm tra xem giao diện này được cấu hình đúng không bằng
lệnh ifconfig. Theo quy ước, địa chỉ mạng sẽ là 127.0.0.0 và địa chỉ
máy cục bộ là 127.0.0.1. Nếu mọi thứ được cấu hình đúng, bạn sẽ có thể
telnet tới chính máy bạn và có thể đăng nhập thông qua telnet.</para>
	  <para>Hãy kiểm tra để chắc chắn
<filename>/etc/hosts.lpd</filename> chứa tên máy của bạn.</para>
	  <para>If your machine has a network-aware
<command>lpd</command>, like the one that comes with LPRng, make sure
that <filename>/etc/lpd.perms</filename>  is configured correctly.
</para>
	  <para>Ngoài ra hãy xem <ulink
url="http://www.tldp.org/HOWTO/Printing-HOWTO/index.html"><citetitle>Printing
HOWTO</citetitle></ulink>. <xref
	      	      linkend="howtos-and-other-documentation"/>.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="timestamps-on-files-are-incorrectly"><para>Tại
sao Timestamps tên tập tin trên partition MS-DOS không
đúng?</para></question>
	<answer>
	  <para>Có một lỗi trong chương trình <command>clock</command>
(thường được đặt trong <filename>/sbin</filename>). Nó đếm thiếu một
múi giờ, lẫn lột giây với phút hoặc với thứ khác. Hãy dùng phiên bản
mới hơn.  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="lilo-boot-kernel-image"><para>Làm thế nào để
LILO khởi động kernel image?</para></question>
	<answer>
	  <para>Từ phiên bản hạt nhân 1.1.80 trở đi, các  kernel image
nén, được dùng bởi LILO, đặt tại [arch/i386/boot/zImage], hoặc
[arch/i386/boot/bzImage] khi được tạo ra, và thường được lưu trong thư
mục <filename>/boot/</filename>. Tập tin
<filename>/etc/lilo.conf</filename> thường dùng liên kết biểu tượng
<filename>vmlinuz</filename> symbolic link, không phải là kernel image
thật sự.  </para>
	  <para>Điều này được thay đổi để việc tạo kernel cho các bộ
xử lý khác nhau dễ dàng hơn từ một source tree.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="make-sure-it-boots"><para>Làm thế nào để bảo đảm
hệ thống sẽ khởi động sau khi cài đặt lại hệ điều
hành?</para></question>
	<answer>
	  <para>
Chỉ dẫn này sẽ làm việc bất kể bạn cài đặt lại Linux hay các hệ điều
hành khác:
</para>

	  <itemizedlist>
	    <listitem>
	      <para>Đút đĩa trắng, đã định dạng vào ổ đĩa A: </para>
	    </listitem>
	    <listitem>
	      <para>Lưu bản sao của Master Boot Record (MBR) của đĩa
cứng vào đĩa mềm bằng lệnh:</para>
	      <screen> # dd if=/dev/hda of=/dev/fd0 count=1</screen>

	      <para>
<literal>dd</literal> là một chương trình chuẩn trên hệ thống Linux.
Phiên bản cho MS-Windows có tại <ulink
url="ftp://ftp.gnu.org/">ftp://ftp.gnu.org/</ulink>, cũng như tại
nhiều MS software archive khác.  </para>
	    </listitem>
	    <listitem>
	      <para>Kiểm tra xem đĩa mềm có khởi động hệ thống không,
bằng cách khởi động lại hệ thống khi vẫn để đĩa trong ổ đĩa A:.</para>
	    </listitem>
	    <listitem>
	      <para>Sau đó bạn có thể cài đặt hệ điều hành khác (trên
đĩa cứng khác và/hoặc trên partition khác, nếu bạn không muốn gỡ bỏ
Linux).</para>
	    </listitem>
	    <listitem>
	      <para>Sau khi cài đặt, hãy khởi động Linux bằng đĩa mềm
và cài đặt lại MBR bằng lệnh: <filename>/sbin/lilo</filename>.
</para>
	    </listitem>
	  </itemizedlist>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="upgraded-kernel-pcmcia-doesnt-work"><para>Tại
sao card PCMCIA của tôi không hoạt động sau khi nâng cấp hạt
nhân?</para></question>
	<answer>
	  <para>Môđun dịch vụ Card PCMCIA, nằm trong
<filename>/lib/modules/</filename><emphasis
role='bold'>version</emphasis><filename>/pcmcia</filename>,  với
<emphasis role='bold'>version</emphasis> là phiên bản của kernel, dùng
thông tin cấu hình riêng cho kernel image. Các môđun PCMCIA sẽ không
hoạt động với kernel khác. Bạn cần nâng cấp môđun card PCMCIA khi nâng
cấp kernel.</para>
	  <para>Khi nâng cấp từ kernel cũ, chắc rằng bạn có phiên bản
thư viện run-time, gói module, .. mới nhất (hoặc khá mới). Tham khảo
tập tin <filename>Documentation/Changes</filename> trong mã nguồn
kernel để biết chi tiết.  </para>
	  <para><important>
	      <para>Nếu bạn dùng Dịch vụ Card PCMCIA, đừng bật tùy
chọn <literal>Network device  support/Pocket và portable
adapters</literal> trong menu cấu hình kernel, vì nó xung đột với các
môđun trong Dịch vụ Card PCMCIA.  </para>
	    </important></para>
	  <para>Biết các mối liên hệ môđun PCMCIA trong kernel cũ rất
hữu dụng. Bạn cần lưu thông tin đó lại. Ví dụ, nếu card PCMCIA của bạn
phụ thuộc vào thiết bị cổng tuần tự được cài đặt bằng môđun trên
kernel cũ, bạn cần đảm bảo môđun tuần tự đó cũng có trong kernel mới
luôn.</para>
	  <!-- para>Những thủ tục được mô tả ở đây  kludgey, but it is much easier than re-calculating module dependencies from scratch, và making sure the upgrade modules get  loaded so that both the non-PCMCIA và PCMCIA are happy. Recent kernel  releases contain a myriad of module options, too many to keep track of  easily. These steps use the existing module dependencies as much as possible,  instead of requiring you to calculate new ones.  </para>
	  <para>However, this procedure does not take into account instances where module dependencies are incompatible from one kernel version to another. In these cases, you'll need  to load the modules yourself with insmod, or adjust the module dependencies  in the [/etc/conf.modules] file. The  [Documentation/modules.txt] file in the kernel source tree  contains a good description of how to use the kernel loadable modules và the  module utilities like <command>insmod</command>, <command>modprobe</command>,  và <command>depmod</command>. [Modules.txt] also contains  a recommended procedure for determining which features to include in a  resident kernel, và which to build as modules.  </para>
	  <para>Essentially, you need to follow these steps when you install a new kernel. </para>

	  <itemizedlist>
	    <listitem><para>Before building the new kernel, make a record with the <command>lsmod</command> command of the module dependencies that  your system currently uses. For example, part of the <command>lsmod</command>  output might look like this:  </para>
	      <screen>
  Module         Pages            Used by
  memory_cs      2                0
  ds             2                [memory_cs]  3
  i82365         4                2
  pcmcia_core    8                [memory_cs ds i82365] 3
  sg             1                0
  bsd_comp       1                0
  ppp            5                [bsd_comp] 0
  slhc           2                [ppp] 0
  serial         8                0
  psaux          1                0
  lp             2                0
 </screen>

	      <para>This tells you for example that the [memory_cs] module needs the ds và [pcmcia_core] modules loaded first. What it doesn't say is  that, in order to avoid recalculating the module dependencies, you may also  need to have the [serial], [lp], [psaux], và other standard modules available to prevent  errors when installing the pcmcia routines tại boot time with  <command>insmod</command>. A glance tại the <filename>/etc/modules</filename>  file will tell you what modules the system currently loads, và in what  order. Save a copy of this file for future reference, until you have  successfully installed the new kernel's modules. Also save the  <command>lsmod</command> output to a file, for example, with the command:  <command>lsmod >lsmod.old-kernel.output</command>.  </para>
	    </listitem>
	    <listitem><para>Build the new kernel, và install the boot image, either <filename>zImage</filename> or <filename>bzImage</filename>, to a floppy  diskette. To do this, change to the [arch/i386/boot]  directory (substitute the correct architecture directory if you don't have an  Intel machine), and, with a floppy in the diskette drive, execute the  command:  </para>
	      <screen> $ dd if=bzImage of=/dev/fd0 bs=512</screen>

	      <para>if you built the kernel with the <command>make bzImage</command> command, và if your floppy drive is  <filename>/dev/fd0</filename>. This results in a bootable kernel image being  written to the floppy, và allows you to try out the new kernel without  replacing the existing one that LILO boots on the hard drive.  </para>
	    </listitem>
	    <listitem><para>Boot the new kernel from the floppy to make sure that it works. </para>
	    </listitem>
	    <listitem><para>With the system running the new kernel, compile và install a current version of the PCMCIA Card Services package, available from metalab.unc.edu as well as other Linux  archives. Before installing the Card Services utilities, change the names of  <filename>/sbin/cardmgr</filename> và <filename>/sbin/cardctl</filename> to [/sbin/cardmgr.old] và  [/sbin/cardctl.old]. The old versions of these utilities  are not compatible with the replacement utilities that Card Services  installs. In case something goes awry with the installation, the old  utilities won't be overwritten, và you can revert to the older versions if  necessary. When configuring Card Services with the <quote><literal>make  config</literal></quote> command, make sure that the build scripts know where to  locate the kernel configuration, either bởi using information from the running  kernel, or telling the build process where the source tree of the new kernel  is. The <quote><literal>make config</literal></quote> step should complete without  errors. Installing the modules from the Card Services package places them in  the directory <filename>/lib/modules/</filename><emphasis role='bold'>version</emphasis><filename>/pcmcia</filename>,  where <emphasis role='bold'>version</emphasis> is the version number of the new kernel.  </para>
	    </listitem>
	    <listitem><para>Reboot the system, và note which, if any, of the PCMCIA devices work. Also make sure that the non-PCMCIA hardware  devices are working. It's likely that some or all of them won't work. Use  <command>lsmod</command> to determine which modules the kernel loaded tại boot  time, và compare it with the module listing that the old kernel loaded,  which you saved from the first step of the procedure. (If you didn't save a  listing of the <command>lsmod</command> output, go back và reboot the old  kernel, và make the listing now.)  </para>
	    </listitem>
	    <listitem><para>When all modules are properly loaded, you can replace the old kernel image on the hard drive. This will most likely be the file pointed to bởi the  <filename>/vmlinuz</filename> symlink. Remember to update the boot sector bởi  running the <command>lilo</command> command after installing the new kernel  image on the hard drive.  </para>
	    </listitem>
	    <listitem><para>Also look tại the questions, How do I upgrade/recompile my kernel? và Modprobe can't locate module, "XXX," và similar messages.  </para>
	    </listitem>
	  </itemizedlist -->
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="ls-colors"><para>Làm thế nào để loại bỏ (hoặc
thay đổi) màu sắc trong cách hiển thị của lệnh
<application>ls</application>?</para></question>
	<answer>
	  <para>
Nếu <command>ls</command> đang hiển thị màu và bạn không muốn thế, có
lẽ bạn đang dùng alias của lệnh này. Vài bản phân phối mặc định dùng
cách này. </para>
	  <para>Lệnh shell, <quote><literal>unalias
ls</literal></quote>, sẽ bỏ alias mà bản phân phối dùng.</para>
	  <para>Để dùng lâu dài, hãy sửa script khởi động của bạn,
<filename>.bashrc</filename>. </para>
	</answer>
	<answer>
	  <para>
Để thay đổi màu sắc hơn là bỏ màu, hãy xem man page của
<application>ls</application> man page (<quote><literal>man
ls</literal></quote>).  </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="programs-in-cwd"><para>Tại sao chương trình
không hoạt động trong thư mục hiện thời?</para></question>
	<answer>
	  <para>
Vì thư mục hiện thời không nằm trong đường dẫn tìm kiếm, vì lý do bảo
mật, cũng như để đảm bảo bạn dùng bạn dùng đúng phiên bản của các
chương trình. Nếu một kẻ xâm nhập có thể tạo một tập tin (một chương
trình) trong thư mục công cộng, như <filename>/tmp</filename>, người
đó sẽ có thể chạy chương trình đó nếu nó nằm trong đường dẫn tìm kiếm.
Giải pháp cho vấn đề này là gộp cả thư mục khi gọi lệnh; v.d., dủng
<quote><literal>./myprog</literal></quote>, thay vì
<quote><literal>myprog</literal></quote>. Hoặc là thêm thư mục hiện
thời vào biến môi trường <literal>PATH</literal>; v.d.,
<quote><literal>export  PATH=".:"$PATH</literal></quote> trong
<application>bash</application>, mặc dù cách này không được khuyến
khích vì lý do nêu trên.</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="less-usage">
	  <para>Sử dụng <command>less</command> như thế nào?</para>
	</question>
	<answer>
	  <para>Có vài phím cần nhớ để dùng
<command>less</command>:</para>
	  <para><itemizedlist>
	      <listitem>
		<para>Nhấn phím <keycap>h</keycap> để biết trợ giúp về
<command>less</command></para>
	      </listitem>
	      <listitem>
		<para>Dùng phím <keycap>q</keycap> để thoát khỏi
<command>less</command></para>
	      </listitem>
	      <listitem>
		<para>Nếu mở nhiều tập tin thì dùng
<userinput>:n</userinput> và <userinput>:p</userinput> để xem tập tin
kế tiếp hay tập tin trước đó.</para>
	      </listitem>
	      <listitem>
		<para>Dùng <userinput><keycombo> <keycap>Ctrl</keycap>
		      <keycap>G</keycap> </keycombo> để biết thông tin
về tập tin đang xem. </userinput></para>
	      </listitem>
	      <listitem>
		<para>Nếu <keycap>Home</keycap> hoặc
<keycap>End</keycap> không hoạt động thì có thể dùng<keycap>g</keycap>
hoặc <keycap>G</keycap> tương ứng.</para>
	      </listitem>
	      <listitem>
		<para>Tìm kiếm trong <command>less</command> bằng cách
gõ <keycap>/</keycap> rồi gõ chuỗi cần tìm (biểu thức chính quy). Nếu
muốn dùng chuỗi thường thì gõ thêm <keycombo>
		    <keycap>Ctrl</keycap>
		    <keycap>R</keycap>
		  </keycombo>. Bật/tắt phân biệt chữ hoa/chữ thường
bằng <userinput>-I</userinput>.
</para>
	      </listitem>
	    </itemizedlist>
</para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="man-usage">
	  <para>Sử dụng <command>man</command> như thế nào?</para>
	</question>
	<answer>
	  <para>Đây là những tập tin trợ giúp chuẩn, có trong hầu hết
các chương trình Linux (không tính các chương trình thuộc
<acronym>GNOME</acronym> hoặc <acronym>KDE</acronym>).</para>
	  <para><userinput>man tên-lệnh</userinput> sẽ cho bạn biết
thông tin về lệnh đó.</para>
	  <para><userinput>man -f têm-lệnh</userinput> đưa ra mộ tả
ngắn gọn (một dòng) về lệnh đó.</para>
	  <para><userinput>man -k chuỗi-mô-tả</userinput> liệt kê
những lệnh có vẻ liên quan đến <literal>chuỗi-mô-tả</literal> bằng
cách tìm chuỗi đó trong phần synopsis.</para>
	  <para><userinput>man -K chuỗi-mô-tả</userinput> cũng như
trên, nhưng tìm trong toàn bộ man page thay vì chỉ dòng synopsis (và
dĩ nhiên chậm hơn).</para>
	  <para>Sử dụng <command>man</command> cũng như
<command>less</command>. Xem <xref 	      linkend="less-usage"/> </para>
	</answer>
      </qandaentry>
      <qandaentry>
	<question id="info-usage">
	  <para>Sử dụng <command>info</command> như thế nào?</para>
	</question>
	<answer>
	  <para>Trước hết, gõ <userinput>info info</userinput> để
<command>info</command> tự giới thiệu với bạn.</para>
	  <para>Dùng phím <keycap>n</keycap> để đến trang kế tiếp,
<keycap>p</keycap> để đến trang trước đó,<keycap>u</keycap> để đến
trang cha,<keycap>l</keycap> để về trang vừa
xem,<keycap>Enter</keycap> để xem một liên kết (có dạng <quote>* cái
gì đó ở đây::</quote>), và dĩ nhiên <keycap>q</keycap> để
thoát.</para>
	</answer>
      </qandaentry>
    </qandaset>
  </sect1>

  <!-- Local Variables: -->
  <!-- coding: vietnamese-utf8 -->
  <!-- sgml-parent-document: "Linux-FAQ.xml" -->
  <!-- End: -->
