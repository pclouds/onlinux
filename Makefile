all: contribution.pdf l10n.pdf

GIT-VERSION-FILE: .FORCE-GIT-VERSION-FILE
	$(SHELL_PATH) ./GIT-VERSION-GEN
-include GIT-VERSION-FILE

%.tex2: %.tex
	sed "s,@@revision@@,$(GIT_VERSION)," $< > $@

%.pdf: %.tex2
	pdflatex $<

#%.pdf: %.tex
#	pdflatex $<

.PHONY: .FORCE-GIT-VERSION-FILE
